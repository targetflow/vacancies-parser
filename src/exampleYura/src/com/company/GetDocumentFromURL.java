package exampleYura.src.com.company;
import java.io.IOException;
import java.util.Scanner;
import model.Vacancy;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class GetDocumentFromURL {
    private static Vacancy[] vacObject;

    private static void Parse(String strVacancy) throws IOException {
        String str = "";
        strVacancy = strVacancy.replace("#", "%23");
        strVacancy = strVacancy.replace(" ", "+");

        int vacancyCount = 0;

        Document dou = Jsoup.connect("https://jobs.dou.ua/vacancies/?search=" + strVacancy).get();

        Elements resultOfSearch = dou.select("title");
        Elements elements = resultOfSearch.select("a");

        for (Element element : elements) {
            String outStr = element.absUrl("href");
            String vacancyName = dou.select("f-visited-enable ga_listing").text();
            String vacancyCompany = dou.select("company").text();
            String vacancyCity = dou.select("cities").text();
            if (!(vacObject == null)) {
                vacObject[vacancyCount] = new Vacancy(
                        outStr,
                        vacancyName,
                        vacancyCompany,
                        vacancyCity
                );
            }

            vacancyCount++;
        }
    }


    private static void printVacanciesInfo() {
        for (int i = 0; i < Vacancy.getCount(); i++) {
            System.out.println(vacObject[i].getVacancyLink() +
                    "\nName: " + vacObject[i].getVacancyName() +
                    "\nCompany: " + vacObject[i].getVacancyCompany() +
                    "\nDate: " + vacObject[i].getVacancyDate() +
                    "\nSalary: " + vacObject[i].getVacancySalary() +
                    "\nCity: " + vacObject[i].getVacancyCity() +
                    "\nEmployment: " + vacObject[i].getVacancyEmployment() +
                    "\nRequirements: " + vacObject[i].getVacancyRequirements() + "\n"
            );
        }
    }

    /*
    public static void main(String[] args) throws IOException {

        Scanner in = new Scanner(System.in);
        System.out.print("Enter job title: ");
        String strVacancy = in.nextLine();

        System.out.print("Enter the number of pages: ");
        Integer countPages = in.nextInt() + 1;

        Parse(strVacancy);

        printVacanciesInfo();
        System.out.println("Vacancy count - " + Vacancy.getCount() + "!");
    }
    */
}
