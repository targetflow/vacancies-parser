package model;

public class Vacancy {
    private static Integer count = 0;

    private String website;
    private String vacancyLink;
    private String vacancyName;
    private String vacancyCompany;
    private String vacancyDate;
    private String vacancySalary;
    private String vacancyCity;
    private String vacancyEmployment;
    private String vacancyRequirements;

    private String[] siteName = new String[] {"work.ua", "jobs.dou.ua", "rabota.ua"};

    // Default constructor
    public Vacancy(String outStr, String str, String vacancyName, String vacancyCompany, String vacancyCity) {}

    // Constructor for Maksim
    public Vacancy(String vacancyLink, String vacancyName, String vacancyCompany, String vacancyDate,
                   String vacancySalary, String vacancyCity, String vacancyEmployment, String vacancyRequirements) {
        this.website = siteName[0]; // work.ua
        this.vacancyLink = vacancyLink;
        this.vacancyName = vacancyName;
        this.vacancyCompany = vacancyCompany;
        this.vacancyDate = vacancyDate;
        this.vacancySalary = vacancySalary;
        this.vacancyCity = vacancyCity;
        this.vacancyEmployment = vacancyEmployment;
        this.vacancyRequirements = vacancyRequirements;

        count++;
    }

    // Constructor for Yura
    public Vacancy(String vacancyLink, String vacancyName, String vacancyCompany, String vacancyCity) {
        this.website = siteName[1]; // jobs.dou.ua
        this.vacancyLink = vacancyLink;
        this.vacancyName = vacancyName;
        this.vacancyCompany = vacancyCompany;
        this.vacancyCity = vacancyCity;

        count++;
    }

    // Constructor for Serg
    public Vacancy(String vacancyLink, String vacancyName) {
        this.website = siteName[2]; // rabota.ua
        this.vacancyLink = vacancyLink;
        this.vacancyName = vacancyName;
        // ...

        count++;
    }

    public String getWebsite() {
        return website;
    }

     /*
        Setter for website(setWebsite) do not needs.
    */

    public String getVacancyLink() {
        return vacancyLink;
    }

    public void setVacancyLink(String vacancyLink) {
        this.vacancyLink = vacancyLink;
    }

    public String getHref() {
        return vacancyLink;
    }

    public void setHref(String href) {
        this.vacancyLink = href;
    }

    public String getVacancyName() {
        return vacancyName;
    }

    public void setVacancyName(String vacancyName) {
        this.vacancyName = vacancyName;
    }

    public String getVacancyCompany() {
        return vacancyCompany;
    }

    public void setVacancyCompany(String vacancyCompany) {
        this.vacancyCompany = vacancyCompany;
    }

    public String getVacancyDate() {
        return vacancyDate;
    }

    public void setVacancyDate(String vacancyDate) {
        this.vacancyDate = vacancyDate;
    }

    public String getVacancySalary() {
        return vacancySalary;
    }

    public void setVacancySalary(String vacancySalary) {
        this.vacancySalary = vacancySalary;
    }

    public String getVacancyCity() {
        return vacancyCity;
    }

    public void setVacancyCity(String vacancyCity) {
        this.vacancyCity = vacancyCity;
    }

    public String getVacancyEmployment() {
        return vacancyEmployment;
    }

    public void setVacancyEmployment(String vacancyEmployment) {
        this.vacancyEmployment = vacancyEmployment;
    }

    public String getVacancyRequirements() {
        return vacancyRequirements;
    }

    public void setVacancyRequirements(String vacancyRequirements) {
        this.vacancyRequirements = vacancyRequirements;
    }

    // Static allows you to call a method without an object
    public static Integer getCount()
    {
        return count;
    }
}
