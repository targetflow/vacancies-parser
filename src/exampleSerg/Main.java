package exampleSerg;

import java.io.IOException;

import com.google.gson.*;
import main.Vacancy;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.DataNode;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.Scanner;

class Result{
       String vacancy_Name;
       String vacancy_CompanyName;
       String vacancy_CityName;
       String vacancy_VacancyDate;
}
public class Main {
    private static Vacancy[] vacObject;



    private static void Parse(String strVacancy, Integer countPages) throws IOException {
        if(strVacancy.contains("#")) {
            strVacancy = strVacancy.replace("#", "%23");
        }
        else if(strVacancy.contains("++")) {
            strVacancy = strVacancy.replace("++", "%2b%2b");
        }

        Integer page = 1;
        int vacancyCount = 0;
        vacObject = new Vacancy[countPages * (20 + 1)]; // +1 - reserve

        while(page < countPages) {
            Document rabotaua = Jsoup.connect(
                    "https://rabota.ua/jobsearch/vacancy_list?keyWords=" + strVacancy + "&pg=" + page
            ).get();

            Elements resultOfSearch = rabotaua.getElementsByClass("f-visited-enable ga_listing");
            Elements elements = resultOfSearch.select("a");

            for (Element element : elements) {
                Document vacancy = Jsoup.connect(element.absUrl("href")).get();
                String href = element.absUrl("href");
                JsonElement jelement = new JsonParser().parse("ruavars");
                Gson gson = new Gson();
                Result result = gson.fromJson(jelement, Result.class);
                System.out.println(result);
                    String json = vacancy.select("script:contains(var ruavars)").text();
                JSONObject JSON = new JSONObject(json);
//                gson.toJson(json);
//                String vacancyCompany = gson.fromJson("\"vacancy_CompanyName\"", String.class);
//                String vacancyName = gson.fromJson("\"vacancy_Name\"", String.class);
//                String vacancyCity = gson.fromJson("\"vacancy_CityName\"", String.class);
                String vacancyCompany = JSON.getString("vacancy_CompanyName");
                String vacancyName = JSON.getString("vacancy_Name");
                String vacancyCity = JSON.getString("vacancy_CityName");
                vacObject[vacancyCount] = new Vacancy(
                        href,
                        vacancyCompany,
                        vacancyName,
                        vacancyCity
                );

                vacancyCount++;
            }

            page++;
        }
    }
    private static void printVacanciesInfo() {
        for(int i = 0; i < Vacancy.getCount(); i++) {
            System.out.println(
                    "\nName: " + vacObject[i].getVacancyName() +
                            "\nCompany: " + vacObject[i].getVacancyCompany() +
                            "\nCity: " + vacObject[i].getVacancyCity() + "\n"
            );
        }
    }
    public static void main(String[] args) throws IOException {
        Scanner in = new Scanner(System.in);
        System.out.print("Enter job title: ");
        String strVacancy = in.nextLine();

        System.out.print("Enter the number of pages: ");
        Integer countPages = in.nextInt() + 1;

        Parse(strVacancy, countPages);

        printVacanciesInfo();
        System.out.println("Vacancy count - " + Vacancy.getCount() + "!");
    }
}
