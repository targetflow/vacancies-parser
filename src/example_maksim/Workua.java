package example_maksim;

import java.io.IOException;

import model.Vacancy;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

// import java.io.FileWriter;

public class Workua {
    private static Vacancy[] vacObject;

    public static void printVacanciesInfo() {
        for(int i = 0; i < Vacancy.getCount(); i++) {
            System.out.println(vacObject[i].getVacancyLink() +
                    "\nName: " + vacObject[i].getVacancyName() +
                    "\nCompany: " + vacObject[i].getVacancyCompany() +
                    "\nDate: " + vacObject[i].getVacancyDate() +
                    "\nSalary: " + vacObject[i].getVacancySalary() +
                    "\nCity: " + vacObject[i].getVacancyCity() +
                    "\nEmployment: " + vacObject[i].getVacancyEmployment() +
                    "\nRequirements: " + vacObject[i].getVacancyRequirements() + "\n"
            );
        }

        System.out.println("Vacancy count - " + Vacancy.getCount() + "!");
    }

    public static void getInfoWorkUa(String strVacancy, Integer countPages) throws IOException {
        if(strVacancy.contains("#")) {
            strVacancy = strVacancy.replace("#", "%23");
        }

        strVacancy = strVacancy.replace(" ", "+");

        // FileWriter logfile = new FileWriter("logfile.txt");

        Integer page = 1;
        int vacancyCount = 0;
        vacObject = new Vacancy[countPages * (14 + 1)]; // +1 - reserve

        while(page < countPages) {
            // logfile.write("\tСтраница - " + page + "\n\n");
            Document workua = Jsoup.connect(
                    "https://work.ua/jobs-" + strVacancy + "/?page=" + page
            ).get();

            Elements resultOfSearch = workua.getElementsByClass("add-bottom-sm");
            Elements elements = resultOfSearch.select("a");

            for (Element element : elements) {
                Document vacancy = Jsoup.connect(element.absUrl("href")).get();

                String vacancyName = vacancy.select("h1.add-top-sm").text();
                String vacancyCompany = vacancy.select("dl.dl-horizontal a[title]").text();
                String vacancyDate = vacancy.select("p.cut-bottom-print > span.text-muted").text();
                String vacancySalary = vacancy.select("h3.text-muted").text();
                String vacancyCity = "unknown";
                String vacancyEmployment = "unknown";
                String vacancyRequirements = "unknown";

                if(vacancySalary.isEmpty()) {
                    vacancySalary = "unknown";
                }

                for(int i = 0; i < 10; i++) {
                    String tempData = vacancy.select("dl.dl-horizontal dt:eq(" + i + ")").text();

                    int value = i + 1;

                    switch (tempData) {
                        case "Город:":
                            vacancyCity = vacancy.select("dl.dl-horizontal dd:eq(" + value + ")").text();
                            break;
                        case "Вид занятости:":
                            vacancyEmployment = vacancy.select("dl.dl-horizontal dd:eq(" + value + ")").text();
                            break;
                        case "Требования:":
                            vacancyRequirements = vacancy.select("dl.dl-horizontal dd:eq(" + value + ")").text();
                            break;
                    }
                }
                /*
                String outStr = element.absUrl("href") +
                        "\nName: " + vacancyName +
                        "\nCompany: " + vacancyCompany +
                        "\nDate: " + vacancyDate +
                        "\nSalary: " + vacancySalary +
                        "\nCity: " + vacancyCity +
                        "\nEmployment: " + vacancyEmployment +
                        "\nRequirements: " + vacancyRequirements
                        ;

                System.out.println(outStr + "\n");
                logfile.write(outStr + "\n\n"); // Output to file
                */

                // Add object to class Vacancy
                vacObject[vacancyCount] = new Vacancy(
                        element.absUrl("href"),
                        vacancyName,
                        vacancyCompany,
                        vacancyDate,
                        vacancySalary,
                        vacancyCity,
                        vacancyEmployment,
                        vacancyRequirements
                );

                vacancyCount++;
            }

            page++;
        }

        /*
        logfile.write("\nVacancy count - " + vacancyCount + "!");
        logfile.close();
        */
    }

    public static void main(String[] args) {

    }
}